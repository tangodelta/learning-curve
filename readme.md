﻿# Learning Curve -- because learning smart is better than learning hard 

*And the most important possible thing you can do is do a lot of work —
do a huge volume of work. Put yourself on a deadline so that every week,
or every month, you know you’re going to finish one story.*

*Because it’s only by actually going through a volume of work that you 
are actually going to catch up and close that gap.*

*And the work you’re making will be as good as your ambitions.*

*It takes a while, it’s gonna take you a while — it’s normal to take a while.
And you just have to fight your way through that, okay?*

**The Gap -- Ira Glass**   
https://www.brainpickings.org/2014/01/29/ira-glass-success-daniel-sax/

*No problem should ever have to be solved twice.
Creative brains are a valuable, limited resource. 
They shouldn't be wasted on re-inventing the wheel 
when there are so many fascinating new problems waiting out there.*

**How To Become A Hacker -- Eric Steven Raymond**  
http://www.catb.org/~esr/faqs/hacker-howto.html

## Why?

Learning how to draw is a considerable challenge. There is an  
abundance of learning material for those willing to learn,  
but quantity does not translate into quality. 

Even when dealing with resources of solid quality it may be an  
issue to find the right resource for the job. What noobs need
is not only a curated body of knowledge but also guidance.

Everyone was a noob once. I'm a noob, so perhaps my best input  
could be helping others like me.

## How?

One of the myths is that you have to buy an extremely expensive  
piece of kit (tablet, markers etc.) and/or pay an arm and a leg for  
commercial software.

I can **not** in good faith repost such nonsense.

To start digital drawing all you need is Krita https://krita.org/en/  
and a tablet. David Revoy has a solid write-up on tablets:
https://www.davidrevoy.com/article332/tablet-history-log .

As for traditional art: all you need is a bunch of pencils and some paper.

This page and collection of resources will hopefully tell you how  
to use all that hardware and software.
